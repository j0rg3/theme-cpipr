<?php
/*
 * Largo Child DeLaRed Posts
 */
class largo_child_delared_widget extends WP_Widget {

	function largo_child_delared_widget() {
		$widget_ops = array(
			'classname' 	=> 'largo-child-delared',
			'description' 	=> __('Show De la Red posts with thumbnails and excerpts', 'largo')
		);
		$this->WP_Widget( 'largo-child-delared-widget', __('Largo Child De La Red Posts', 'largo'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Network' ) : $instance['title'], $instance, $this->id_base);

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;?>

			<?php
				$args = array(
					'paged'			=> $paged,
					'post_status'	=> 'publish',
					'posts_per_page'=> $instance['num_posts'],
					'category_name' => "network"
					);
				$featured = new WP_Query( $args );
          	if ( $featured->have_posts() ) : ?>
				<?php while ( $featured->have_posts() ) : $featured->the_post(); ?>
                  	<div class="post-lead clearfix">
                      	<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						<a href="<?php the_permalink(); ?>">
                      	<?php the_post_thumbnail( '350x292' ); ?>
						</a>
                     	<?php echo '<p>' . largo_trim_sentences( get_the_content(), $instance['num_sentences'] ) . '</p>'; ?>
                  	</div> <!-- /.post-lead -->
	            <?php endwhile; ?>
				<?php else: ?>
	    		<p class="error"><strong>You don't have any posts in the De la Red category.</strong></p>

    		<?php endif; // end more de la red posts ?>

		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['num_posts'] = strip_tags( $new_instance['num_posts'] );
		$instance['num_sentences'] = strip_tags( $new_instance['num_sentences'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array(
			'title' 			=> __('Network', 'largo'),
			'num_posts' 		=> 5,
			'num_sentences' 	=> 2
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'largo'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'num_posts' ); ?>"><?php _e('Number of posts to show:', 'largo'); ?></label>
			<input id="<?php echo $this->get_field_id( 'num_posts' ); ?>" name="<?php echo $this->get_field_name( 'num_posts' ); ?>" value="<?php echo $instance['num_posts']; ?>" style="width:90%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'num_sentences' ); ?>"><?php _e('Excerpt Length (# of Sentences):', 'largo'); ?></label>
			<input id="<?php echo $this->get_field_id( 'num_sentences' ); ?>" name="<?php echo $this->get_field_name( 'num_sentences' ); ?>" value="<?php echo $instance['num_sentences']; ?>" style="width:90%;" />
		</p>

	<?php
	}
}