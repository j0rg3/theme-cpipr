<?php

//child theme text domain
add_action( 'after_setup_theme', 'cpipr_theme_setup' );
function cpipr_theme_setup() {
    load_child_theme_textdomain( 'cpipr', get_stylesheet_directory() . '/lang' );
}

//use the Largo metabox API
require_once( get_template_directory() . '/largo-apis.php' );

//add a meta box for a subtitle on posts
largo_add_meta_box(
	'subtitle',
	'Subtitle',
	'subtitle_meta_box_display',
	'post',
	'normal',
	'core'
);
function subtitle_meta_box_display() {
	global $post;
	$values = get_post_custom( $post->ID );
	wp_nonce_field( 'largo_meta_box_nonce', 'meta_box_nonce' );
	?>
	<label for="subtitle"><?php _e('Subtitle', 'largo'); ?></label>
	<textarea name="subtitle" id="subtitle" class="widefat" rows="2" cols="20"><?php if ( isset ( $values['subtitle'] ) ) echo $values['subtitle'][0]; ?></textarea>
	<?php
	largo_register_meta_input( 'subtitle' );
}

//add a meta box for a subtitle on posts
largo_add_meta_box(
	'en_version_url',
	'English Version URL',
	'en_version_url_meta_box_display',
	'post',
	'side',
	'default'
);
function en_version_url_meta_box_display() {
	global $post;
	$values = get_post_custom( $post->ID );
	wp_nonce_field( 'largo_meta_box_nonce', 'meta_box_nonce' );
	?>
	<label for="en_version_url"><?php _e('English Version URL (include http://)', 'largo'); ?></label>
	<input name="en_version_url" id="en_version_url" style="width:90%;" value="<?php if ( isset ( $values['en_version_url'] ) ) echo $values['en_version_url'][0]; ?>" style="width:90%;" />
	<?php
	largo_register_meta_input( 'en_version_url' );
}

//register CPIPR widgets
add_action( 'widgets_init', 'cpipr_widgets' );
function cpipr_widgets() {
	require_once( get_stylesheet_directory() . '/inc/widgets/largo-child-DeLaRed.php' );
	register_widget( 'largo_child_delared_widget' );
}

//sidebar for the partners section in the footer
add_action( 'widgets_init', 'cpipr_register_sidebars' );
function cpipr_register_sidebars() {
	register_sidebar( array(
		'name' 			=> __( 'Partners', 'cpipr' ),
		'id' 			=> 'partners',
		'description' 	=> __( 'Partners section in the footer', 'cpipr' ),
		'before_widget' => '',
		'after_widget' 	=> '',
		'before_title' 	=> '',
		'after_title' 	=> '',
	) );
}

//register additional image size used in de la red widget
add_action( 'after_setup_theme', 'cpipr_image_sizes' );
function cpipr_image_sizes() {
	add_image_size( '350x292', 350, 292, true );
}

//load typekit
add_action( 'wp_head', 'cpipr_typekit' );
function cpipr_typekit() { ?>
	<script type="text/javascript" src="//use.typekit.net/dyj6ctr.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php
}

//allow admins to add users without requiring email confirmation
function auto_activate_users($user, $user_email, $key, $meta){
  wpmu_activate_signup($key);
  return false;
}
add_filter( 'wpmu_signup_user_notification', 'auto_activate_users', 10, 4);
add_filter( 'wpmu_signup_user_notification', '__return_false' );

function largo_time( $echo = true ) {
	$time_difference = current_time('timestamp') - get_the_time('U');

	if ( $time_difference < 86400 )
		$output = sprintf( __('<span class="time-ago">%s ago</span>', 'largo' ),
			human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) )
		);
	else
		$output = get_the_date( 'j \d\e F Y' );

	if ( $echo )
		echo $output;
	return $output;
}